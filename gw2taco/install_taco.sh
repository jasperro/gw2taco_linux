#!/bin/bash

# ========================= DESCRIPTION =================================#
# Description: Installer part of the gw2taco_linux package
# Project Link: https://gitlab.com/namoninja/gw2taco_linux
# Author: Namo Ninja
# License: GNU GPL v3+
# Version: 1.3.0
# =======================================================================#

#=============================== VARS ===================================#
VERSION="1.3.0"

# Identify location of where script is located and !not where it started
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
GW2="$(dirname "${DIR}")"
cd "${DIR}"

SETTINGS="config/settings.conf"
LINKS="config/taco.links"

# Get info from ${LINKS}
# TacoPackName
TPN=(A B C D E F)
	for X in ${TPN[*]}
	do
		eval TPN$X=`grep ${X}name== ${LINKS} | sed 's/^.name==//'`;
	done
# TacoPackLocation
TPL=(A B C D E F)
	for X in ${TPL[*]}
	do
		eval TPL$X=`grep ${X}loc== ${LINKS} | sed 's/^.loc==//'`;
	done
# TacoPackSiteurl
TPS=(A B C D E F)
	for X in ${TPS[*]}
	do
		eval TPS$X=`grep ${X}site== ${LINKS} | sed 's/^.site==//'`;
	done
# DownloadLink
DL=(A1 B1 C1 D1 E1 E2 E3 E4 E5 E6 E7 E8 E9 E10 E11 E12 F1)
	for X in ${DL[*]}
	do
		eval DL$X=`grep $X== ${LINKS} | sed 's/^.*==//'`;
	done

# Define placeholders
INSTALLED="Not installed"
c1="---" # distribution
c2="---" # desktop
c3="---" # wine
c4="---" # opacity
c5="---" # autostart
c6="---" # autostarttime
c7="---" # manualstarttime
c8="" # installed (packs)
c9="" # active (packs)
c10="---" # mode

# Define Colors
GC='\e[32m' # Green
RC='\e[91m'	# Red
YC='\e[93m' # Yellow
MC='\e[95m'	# Magenta
BC='\e[94m'	# Blue
CC='\e[96m'	# Cyan
NC='\e[0m'	# No Color

#============================ FUNCTIONS =================================#
#-------------------------- ERROR CHECKS--------------------------------#
check_error()
{
	local check="$1"
	local userinput="$2"
	local exitcode="$2"
	local repeat="$2"
	local section="$3"
	local file="$3"

	case $check in

		"confirm")
			if [[ "$userinput" =~ ^[Yy]$ ]]; then # YES

				if [[ "$section" == "finish" ]]; then
					exit 0
				else
					clear
				fi

			elif [[ "$userinput" =~ ^[Nn]$ ]]; then # NO
				
				if [[ "$section" == "download" ]] || [[ "$section" == "install" ]] || \
					[[ "$section" == "uninstall" ]] || [[ "$section" == "uninstall" ]]; then
					taco_man
				elif [[ "$section" == "finish" ]]; then
					check_install
				else
					printf "${RC} Exiting...${NC}\n\v"
					exit 1
				fi

			else
				printf "${RC}" && read -rep "Enter (y/n): " -n 1 YESNO && printf "${NC}"
				check_error "confirm" "$YESNO" "$section"
			fi
		;;

		"autostart")
			if [[ "$userinput" =~ ^[YyNn]$ ]]; then
				echo ""
			else
				printf "${RC}" && read -rep "Enter (y/n): " -n 1 AUTO && printf "${NC}"
				check_error "autostart" "$AUTO"
			fi
		;;

		"appfolder")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----FAILED to remove app folder (probably did not exist) [err:${NC} $exitcode ${RC}]${NC}\n\v"
			fi
		;;

		"download")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----Download Failed! [err:${NC} $exitcode ${RC}]${NC}\n\v"
				rm $file
				read -n 1 -s -r -p "Press any key to continue"
				check_install
			else
				printf "${GC}----DONE.${NC}\n\v"
			fi
		;;

		"extract")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----ERROR\n Something went wrong during extraction... [err:${NC} $exitcode ${RC}]${NC}\n\v"
				read -n 1 -s -r -p "Press any key to continue"
				check_install
			else
				printf "${GC}----DONE${NC}\n\v"
			fi
		;;

		"activate")
			if [[ "$c8" =~ [$userinput] ]]; then
				echo ""
			else
				printf "${RC} The package needs to be installed before you can activate it!${NC}\n\v"
				sleep 3 && check_install
			fi
		;;

		"apt")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}  Something went wrong with apt [err:${NC} $exitcode ${RC}]\n\v${NC}"
				exit $exitcode
			else
				printf "${GC}  Install complete.${NC}\n\v"
			fi
		;;

		"pacman")
			if [ $exitcode -ne 0 ]; then
				printf "${RC} Something went wrong with pacman! [err:${NC} $exitcode ${RC}]${NC}\n\v"
				exit $exitcode
			else
				printf "${GC}----Install complete.${NC}\n\v"
			fi
		;;

		"tweaks")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----ERROR [err:${NC} $exitcode ${RC}]${NC}\n\v"
			else
				printf "${GC}----DONE${NC}\n\v"
			fi
		;;

		"kwinrules")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----Something went wrong with kwinrules... [err:${NC} $exitcode ${RC}]${NC}\n\v"
				exit $exitcode
			else
				printf "${GC}----DONE${NC}\n\v"
			fi
		;;

		"kwinrulesremove")
			if [ $exitcode -ne 0 ]; then
				printf "${RC}----FAILED to remove kwinrules (probably did not exist) [err:${NC} $exitcode ${RC}]${NC}\n\v"
			fi
		;;

		"disabled")
			printf "${RC}[DISABLED] incomplete function, temporarily disabled.\n"
			sleep 2 && taco_man
		;;
	esac
}
#------------------------------------------------------------------------#
#--------------------------- DEFAULT EXPORTS ---------------------------#
export_defaults()
{
	# Export configs
	printf "${GC}** Exporting user info to${CC} ${SETTINGS}... ${NC}\n"
	printf "version=${VERSION}\ngw2dir=${GW2}\ntacodir=${DIR}\ndistribution=${DIST}\ndesktop=${DE}\ncompositor=${COMP}\nwine=${WINE}" > "${DIR}/${SETTINGS}"
	check_error "tweaks" "$?"
	printf "${GC}** Setting Defaults... ${NC}\n"
	printf "\nautostarttime=120" >> "${SETTINGS}"
	printf " Default autostart time set to 120s\n"
	printf "\nmanualstarttime=5" >> "${SETTINGS}"
	printf " Default manualstart time set to 5s\n"
	if [ "$DE" == "KDE" ]; then
		printf "\nopacity=50" >> "${SETTINGS}"
		printf " Default opacity set to 50\n"
	else
		printf "\nopacity=0.5" >> "${SETTINGS}"
		printf " Default opacity set to 0.5\n"
	fi
	printf "\nmode=opacity" >> "${SETTINGS}"
	printf "  Default Tweak MODE set to use OPACITY\n"
	check_error "tweaks" "$?"
	printf "\ninstalled=" >> "${SETTINGS}"
	printf "\nactive=" >> "${SETTINGS}"
}

#-----------------------------------------------------------------------#
#-------------------------TACO MENU TRANSPARENCY FIX --------------------#
fix_transparent_css()
{
	local css_location="$1"
	printf "${GC}*** Applying Transparency Patch for Taco styles...${NC}\n\v"
	sed -z 's/contextmenu.*contextmenu:hover/contextmenu{text-transform:none;border:1px;border-color:#808080;background:#010101;opacity:0.7;}\ncontextmenu:hover/' -i ${css_location}/UI_*.css &&
	printf "${GC}--- DONE!${NC}\n\v"
}

#------------------------------------------------------------------------#
#--------------------------- TACO MAN ----------------------------------#
taco_man()
{
	# Counters
	i=0
	o=0
	tp=("$TPNA" "$TPNB" "$TPNC" "$TPND" "$TPNE" "$TPNF")

	# Header
	show_header "tacoman"
	printf "${NC}         Select an available ${YC}option${NC} OR ${CC}package${NC}\n"
	printf "${NC}      Pressing any other keys will refresh PacMan${NC}\n\v"
	printf "${YC}        OPTION: ${YC}[$o]${NC} Exit\n"  && ((++o))
	if [ "${INSTALLED}" == "Not installed" ]; then
		printf "${YC}        OPTION: ${YC}[$o]${NC} Restart\n"
	else
		printf "${YC}        OPTION: ${YC}[$o]${NC} Uninstall/Reset\n"
	fi
	((++o))
	printf "${YC}        OPTION: ${YC}[$o]${NC} Update\n\v"  && ((++o))

	# Setup folders
	if [ ! -d "tacopacks" ]; then
		mkdir -p tacopacks/{A,B,C,D,E,F}
	fi
	if [ ! -d "app" ]; then
		mkdir -p app/{A,B,C,D,E,F}
	fi

	# Check tacopacks
	for folder in tacopacks/*; do
		if [[ -z "$(ls -A ./$folder)" ]]; then
			printf "${RC}      NOTFOUND: ${YC}[$o]${RC} ${tp[$i]} ${NC}\n"
		else
			if [[ "$c9" == "${TPN[$i]}" ]]; then
				printf "${BC}        ACTIVE: ${YC}[$o]${BC} ${tp[$i]} ${NC}\n"
			elif [[ "$c8" =~ [${TPN[$i]}] ]]; then
				printf "${GC}     INSTALLED: ${YC}[$o]${GC} ${tp[$i]} ${NC}\n"
			else
				printf "${CC}    DOWNLOADED: ${YC}[$o]${CC} ${tp[$i]} ${NC}\n"
			fi
		fi
		((++o)) && ((++i))
	done
	
	# Footer
	show_footer "tacoman"
	read -rep "[Enter #:]" -n 1 TACOMAN

	# Option
	case $TACOMAN in

		0)
			exit 0
		;;

		1) 
			if [ "${INSTALLED}" == "Not installed" ]; then
				check_install
			else
				check_install "uninstall"
			fi
		;;

		2) 
			show_header "tacoman" "single"
			printf "${GC}This area is reserved for easy updating of the gw2taco_linux module.\n${NC}"
			printf "${GC}Feature will be fully released in v1.4.0, currently you get notified if there is an update when run_taco.sh is initialized.\n\v${NC}"

			printf "${CC}press any key to return to main menu.\n\v${NC}"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				0) # RETURN
					taco_man
				;;

			esac
		;;

		3) # [A] GW2TACO OFFICAL ----------------------
			file="gw2taco-official.zip"

			show_header "tacoman" "single"
			printf "             ${CC} ${TPNA}\n\v"
			printf "           ${TPSA}\n\v${NC}"
			printf "This is the official TacO and standard install.\n${NC}"
			printf "This package is required for some of the other packs.\n\v${NC}"

			show_footer "tacopack"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # DOWNLOAD
					read -rep "Download archive? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "download"

					show_header "tacoman"
					printf "${GC}** Downloading${CC} $file ${GC}*${NC}\n"
					wget -nv --show-progress "$DLA1" -O "$TPLA/$file" 
					check_error "download" "$?" "$TPLA/$file"
				;;

				2) # EXTRACT
					read -rep "Install tacopack? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "install"

					show_header "tacoman"
					printf "${GC}** Extracting ${CC}$file${NC}...\n"
					unzip -q -o "$TPLA/$file" -d "app/A"
					check_error "extract" "$?"

					install_tweaks

					printf "\n${GC}** Marking install...\n"
					sed -i 's/installed=.*/&A,/' ${SETTINGS}
					fix_transparent_css "app/A"
					check_error "tweaks" "$?"
				;;

				3) # ACTIVATE
					check_error "activate" "A"
					sed -i 's/active=.*/active=A/' ${SETTINGS}
					sleep 1 && check_install
				;;

				0) # RETURN
					taco_man
				;;

				*)
					printf "${RC}[OPTION] You must select one of the available options${NC}\n"
					sleep 2 && taco_man
				;;

			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		4) # [B] TEKKIT ----------------------
			file=("gw2taco-official.zip" "gw2taco-tekkit.zip")
			show_header "tacoman" "single"

			printf "            ${CC} ${TPNB} ${NC}\n"
			printf " ${CC} ${TPSB} ${NC}\n\v"
			printf "NOTE: Requires official GW2TacO, which will be downloaded automatically.\n${NC}"
			
			show_footer "tacopack"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # DOWNLOAD
					read -rep "Download archive? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "download"

					show_header "tacoman"
					if [ -f "$TPLA/${file[0]}" ]; then
						printf "${GC}** NOTE:${CC} ${file[0]} ${GC}already exists, skipping it's download...${NC}\n\v"
					else
						printf "${GC}** Downloading...${CC} ${file[0]} ${NC}\n"
						wget -nv --show-progress "$DLA1" -O "$TPLA/${file[0]}"
						check_error "download" "$?" "$TPLA/${file[0]}"
					fi
					printf "${GC}** Downloading...${CC} ${file[1]} ${NC}\n"
					wget -nv --show-progress "$DLB1" -O "$TPLB/${file[1]}"
					check_error "download" "$?" "$TPLB/${file[1]}"
				;;

				2) # EXTRACT
					read -rep "Install tacopack? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "install"

					show_header "tacoman"
					printf "${GC}** Extracting ${CC}${file[0]}${NC}...\n"
					unzip -q -o "$TPLA/${file[0]}" -d "app/B"
					check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[1]}${NC}...\n"
					unzip -q -o "$TPLB/${file[1]}" -d "app/B/POIs"
					check_error "extract" "$?"
					
					install_tweaks

					printf "\n${GC}** Marking install...\n"
					sed -i 's/installed=.*/&B,/' ${SETTINGS}
					fix_transparent_css "app/B"
					check_error "tweaks" "$?"
				;;

				3) # ACTIVATE
					check_error "activate" "B"
					sed -i 's/active=.*/active=B/' ${SETTINGS}
					sleep 1 && check_install
				;;

				0) #RETURN
					taco_man
				;;

				*)
					printf "${RC}[OPTION] You must select one of the available options${NC}\n"
					sleep 2 && taco_man
				;;

			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		5) # [C] ReActiff ----------------------
			file="gw2taco-reactiff.zip"
			show_header "tacoman" "single"

			printf "          ${CC} ${TPNC}\n"
			printf "        ${TPSC} ${NC}\n\v"
			printf "NOTE: Self contained install. Does not require official GW2TacO package.\n${NC}"

			show_footer "tacopack"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # DOWNLOAD
					read -rep "Download archive? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "download"

					show_header "tacoman"
					printf "${GC}** Downloading...${CC} $file ${NC}\n"
					wget -nv --show-progress "$DLC1" -O "$TPLC/$file"
					check_error "download" "$?" "$TPLC/$file"
				;;

				2) # EXTRACT
					read -rep "Install tacopack? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "install"

					show_header "tacoman"
					printf "${GC}** Extracting ${CC}${file}${NC}...\n"
					unzip -q -o "$TPLC/$file" -d "app/C"
					check_error "extract" "$?"
					
					install_tweaks

					printf "\n${GC}** Marking install...\n"
					sed -i 's/installed=.*/&C,/' ${SETTINGS}
					fix_transparent_css "app/C"
					check_error "tweaks" "$?"
				;;

				3) # ACTIVATE
					check_error "activate" "C"
					sed -i 's/active=.*/active=C/' ${SETTINGS}
					sleep 1 && check_install
				;;

				0) # RETURN
					taco_man
				;;

				*)
					printf "${RC}[OPTION] You must select one of the available options${NC}\n"
					sleep 2 && taco_man
				;;

			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		6) # [D] MegaPack ----------------------
			file="gw2taco-megapack.zip"
			show_header "tacoman" "single"

			printf "           ${CC} ${TPND}\n"
			printf " ${TPSD}${NC}\n\v"
			printf "NOTE: Self contained package. Does not require official GW2TacO package.\n${NC}"

			show_footer "tacopack"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # DOWNLOAD
					read -rep "Download archive? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "download"

					show_header "tacoman"
					printf "${GC}** Downloading...${CC} $file ${NC}\n"
					wget -nv --show-progress "$DLD1" -O "$TPLD/$file"
					check_error "download" "$?" "$TPLD/$file"
				;;

				2) # EXTRACT
					read -rep "Install tacopack? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "install"

					show_header "tacoman"
					printf "${GC}*Extracting ${CC}$file${NC}...\n"
					unzip -q -o "$TPLD/$file" -d "app/D"
					check_error "extract" "$?"

					install_tweaks

					printf "\n${GC}** Marking install...\n"
					sed -i 's/installed=.*/&D,/' ${SETTINGS}
					fix_transparent_css "app/D"
					check_error "tweaks" "$?"
				;;

				3) # ACTIVATE
					check_error "activate" "D"
					sed -i 's/active=.*/active=D/' ${SETTINGS}
					sleep 1 && check_install
				;;

				0) # RETURN
					taco_man
				;;

				*)
					printf "${RC}[OPTION] You must select one of the available options${NC}\n"
					sleep 2 && taco_man
				;;
			
			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		7) # [E] TISCAN ----------------------
			file=("gw2taco-official.zip" "gw2taco-tiscan1.zip" "gw2taco-tiscan2.zip" "gw2taco-tiscan3.zip" "gw2taco-tiscan4.zip" \
			"gw2taco-tiscan5.zip" "gw2taco-tiscan6.zip" "gw2taco-tiscan7.zip" "gw2taco-tiscan8.zip" \
			"gw2taco-tiscan9.zip" "gw2taco-tiscan10.zip" "gw2taco-tiscan11.zip" "gw2taco-tiscan12.zip")
			show_header "tacoman" "single"

			printf "           ${CC} ${TPNE}\n"
			printf "     ${TPSE} ${NC}\n\v"
			printf "NOTE: Requires official GW2TacO, which will be downloaded automatically.\n${NC}"
			printf "NOTE: Has multiple packages.\n${NC}"

			show_footer "tacopack"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # DOWNLOAD
					printf "              " & read -rep "Download archive? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "download"

					show_header "tacoman"
					if [ -f "$TPLA/${file[0]}" ]; then
						printf "${GC}** NOTE:${CC} ${file[0]} ${GC}already exists, skipping it's download...${NC}\n\v"
					else
						printf "${GC}** Downloading...${CC} ${file[0]} ${NC}\n"
						wget -nv --show-progress "$DLA1" -O "$TPLA/${file[0]}"
						check_error "download" "$?" "$TPLA/${file[0]}"
					fi
					printf "${GC}** Downloading...${CC} gw2taco-tiscan.zip(s) ${NC}\n"
					wget -nv --show-progress "$DLE1" -O "${TPLE}/${file[1]}"; check_error "download" "$?" "${TPLE}/${file[1]}"
					wget -nv --show-progress "$DLE2" -O "${TPLE}/${file[2]}"; check_error "download" "$?" "${TPLE}/${file[2]}"
					wget -nv --show-progress "$DLE3" -O "${TPLE}/${file[3]}"; check_error "download" "$?" "${TPLE}/${file[3]}"
					wget -nv --show-progress "$DLE4" -O "${TPLE}/${file[4]}"; check_error "download" "$?" "${TPLE}/${file[4]}"
					wget -nv --show-progress "$DLE5" -O "${TPLE}/${file[5]}"; check_error "download" "$?" "${TPLE}/${file[5]}"
					wget -nv --show-progress "$DLE6" -O "${TPLE}/${file[6]}"; check_error "download" "$?" "${TPLE}/${file[6]}"
					wget -nv --show-progress "$DLE7" -O "${TPLE}/${file[7]}"; check_error "download" "$?" "${TPLE}/${file[7]}"
					wget -nv --show-progress "$DLE8" -O "${TPLE}/${file[8]}"; check_error "download" "$?" "${TPLE}/${file[8]}"
					wget -nv --show-progress "$DLE9" -O "${TPLE}/${file[9]}"; check_error "download" "$?" "${TPLE}/${file[9]}"
					wget -nv --show-progress "$DLE10" -O "${TPLE}/${file[10]}"; check_error "download" "$?" "${TPLE}/${file[10]}"
					wget -nv --show-progress "$DLE11" -O "${TPLE}/${file[11]}"; check_error "download" "$?" "${TPLE}/${file[11]}"
					wget -nv --show-progress "$DLE12" -O "${TPLE}/${file[12]}"; check_error "download" "$?" "${TPLE}/${file[12]}"
				;;

				2) # EXTRACT
					read -rep "Install tacopacks? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "install"

					show_header "tacoman"
					printf "${GC}** Extracting ${CC}${file[0]}${NC}...\n"
					unzip -q -o "$TPLA/${file[0]}" -d "app/E"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[1]}${NC}...\n"
					unzip -q -o "$TPLE/${file[1]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[2]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[2]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[3]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[3]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[4]}${NC}...\n"
					unzip -q -o "$TPLE/${file[4]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[5]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[5]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[6]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[6]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[7]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[7]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[8]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[8]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[9]}${NC}...\n"
					printf "${RC}----Skipping because file containes backslashes...\n\v"
					#unzip -q -o "$TPLE/${file[9]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[10]}${NC}...\n"
					unzip -q -o "$TPLE/${file[10]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[11]}${NC}...\n"
					unzip -q -o "$TPLE/${file[11]}" -d "app/E/POIs"; check_error "extract" "$?"

					printf "${GC}** Extracting ${CC}${file[12]}${NC}...\n"
					unzip -q -o "$TPLE/${file[12]}" -d "app/E/POIs"; check_error "extract" "$?"
					
					install_tweaks

					printf "\n${GC}** Marking install...\n"
					sed -i 's/installed=.*/&E,/' ${SETTINGS}
					fix_transparent_css "app/E"
					check_error "tweaks" "$?"
				;;

				3) # ACTIVATE
					check_error "activate" "E"
					sed -i 's/active=.*/active=E/' ${SETTINGS}
					sleep 1 && check_install
				;;

				0) # RETURN
					taco_man
				;;

				*)
					printf "${RC}[OPTION] You must select one of the available options${NC}\n"
					sleep 2 && taco_man
				;;

			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		8) # [F] CUSTOM PACK ----------------------
			file="gw2taco-custom.zip"
			show_header "tacoman" "single"

			printf "           ${CC}${TPNF}\n"
			printf "    ${TPSF}${NC}\n\v"
			printf "NOTE: Update ${LINKS} file and refresh this page to load the link.\n\v${NC}"

			show_footer "tacopack"
			read -rep "[Enter #:]" -n 1 OPTION
			case $OPTION in

				1) # DOWNLOAD
					read -rep "Download archive? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "download"

					show_header "tacoman"
					printf "${GC}** Downloading...${CC} $file ${NC}\n"
					wget -nv --show-progress "$DLF1" -O "$TPLF/$file"
					check_error "download" "$?" "$TPLF/$file"
				;;

				2) # EXTRACT
					read -rep "Install tacopack? (y/n) " -n 1 YESNO
					check_error "confirm" "$YESNO" "install"

					show_header "tacoman"
					printf "${GC}** Extracting ${CC}${file}${NC}...\n"
					unzip -q -o "$TPLF/${file}" -d "app/F/POIs"
					check_error "extract" "$?"
					
					install_tweaks

					printf "\n${GC}** Marking install...\n"
					sed -i 's/installed=.*/&F,/' ${SETTINGS}
					fix_transparent_css "app/F"
					check_error "tweaks" "$?"
				;;

				3) # ACTIVATE
					check_error "activate" "F"
					sed -i 's/active=.*/active=F/' ${SETTINGS}
					sleep 1 && check_install
				;;

				0) # RETURN
					taco_man
				;;

				*)
					printf "${RC}[OPTION] You must select one of the available options${NC}\n"
					sleep 2 && taco_man
				;;

			esac
			read -n 1 -s -r -p "Press any key to continue"
			check_install
		;;

		*)
			check_install
		;;

	esac
}
#------------------------------------------------------------------------#
#------------------------- CLEAR & DISPLAY HEADER-------------------------#
show_header()
{	
	local header="$1"
	local header2="$2"
	clear

	printf "${GC}===========================================================\n"
	if [ "$header" == "tacoman" ]; then
		printf ":   	   WELCOME TO THE${CC} GW2TacO_Linux${GC} MANAGER	          :\n"
	else
		printf ":   	   WELCOME TO THE${CC} GW2TacO_Linux${GC} INSTALLER	  :\n"
	fi
	printf ":   							  :\n"
	printf ":		     ( Version: ${YC}${VERSION}${GC} )		          :\n"
	printf ": 	${BC} https://gitlab.com/namoninja/gw2taco_linux${GC} 	  :\n"
	printf "===========================================================${NC}\n\v"

	if [ "$header" == "tacoman" ]; then
		printf "${GC} Distribution: ${NC}$c1${GC} Desktop: ${NC}$c2${GC}\n"
		printf "${GC} Tweaks: ${NC}${INSTALLED}${GC} WINE: ${NC}$c3${NC}\n"
		printf "${GC} MODE: ${NC}$c10${GC} Autostart: ${NC}$c5${NC}\n"
		printf "${GC} OpacityLevel: ${NC}$c4${GC} AutoDelay: ${NC}$c6${GC} ManualDelay: ${NC}$c7${NC}\n\v"

		if [ "$header2" == "installer" ]; then
			printf "${GC}  --------------------${YC} Taco Installer${GC} --------------------${NC}\n\v"
		elif [ "$header2" == "complete" ]; then
			printf "${GC}  --------------------${YC} Taco Installer${GC} --------------------${NC}\n\v"
			printf "${GC}\n------------ Installation complete ------------${NC}"
		else
			printf "${GC}  --------------------${YC} Taco Manager${GC} --------------------${NC}\n\v"
		fi

	fi
}
#------------------------------------------------------------------------#
#--------------------------- DISPLAY FOOTER ----------------------------#
show_footer() {
	local footer="$1"

	if [ "$footer" == "tacoman" ]; then
		echo ""
		printf "${GC}  -----------------------------------------------------${NC}\n\v"
		printf "${YC} [OPTION] ${RC}[NOTFOUND] ${CC}[DOWNLOADED] ${GC}[INSTALLED] ${BC}[ACTIVE] ${NC}\n\v"
	
	elif [ "$footer" == "tacopack" ]; then
		printf "\n	${YC}[ 0 ] Back\n"
		printf "	${YC}[ 1 ]${GC} Download\n"
		printf "	${YC}[ 2 ]${GC} Install\n"
		printf "	${YC}[ 3 ]${BC} Activate\n\v"
		printf "${GC}  -----------------------------------------------------${NC}\n"
	fi
}
#------------------------------------------------------------------------#
#------------------- CHECK IF INSTALLED AND OFFER RESET ----------------#
check_install()
{
	local action="$1"
	show_header "tacoman" "installer"

	# If ${SETTINGS} exists
	if [ -f "${SETTINGS}" ]; then
		
		# Store info in vars
		INSTALLED="Installed"
		c1=`grep 'distribution=' ${SETTINGS} | sed 's/^.*=//'`
		c2=`grep 'desktop=' ${SETTINGS} | sed 's/^.*=//'`
		c3=`grep 'wine=' ${SETTINGS} | sed 's/^.*=//'`
		c4=`grep 'opacity=' ${SETTINGS} | sed 's/^.*=//'`
		c5=`grep 'autostart=' ${SETTINGS} | sed 's/^.*=//'`
		c6=`grep 'autostarttime=' ${SETTINGS} | sed 's/^.*=//'`
		c7=`grep 'manualstarttime=' ${SETTINGS} | sed 's/^.*=//'`
		c8=`grep 'installed=' ${SETTINGS} | sed 's/^.*=//'`
		c9=`grep 'active=' ${SETTINGS} | sed 's/^.*=//'`
		c10=`grep 'mode=' ${SETTINGS} | sed 's/^.*=//'`

		# If chooses to reset
		if [ "$action" == "uninstall" ]; then

			read -rep "Uninstall tacopacks & reset tweaks? (y/n) " -n 1 YESNO
			check_error "confirm" "$YESNO" "uninstall"

			show_header "tacoman"
			printf "${GC}\v  REMOVING APP FOLDER${NC}\n"
			rm -r app
			check_error "appfolder" "$?"

			# If using portable package
			if [ "`cat ${SETTINGS} | grep -o portable`" == "portable" ]; then
				# Unlink from portable package
				printf "${GC}  REMOVING LINK FROM ${NC}../bin/user_run\n"
				sed -i 's/\ \&\ \.\.\/\.\.\/\.\.\/gw2taco\/run_taco.sh//' ../bin/user_run
			fi
			
			# If using KDE
			if [ "`cat ${SETTINGS} | grep -o KDE`" == "KDE" ]; then
				
				# Remove KDE Entries
				printf "${GC}  REMOVING kwinrules${NC}\n"
				COUNT=`grep 'kwinrule=' ${SETTINGS} | sed 's/^.*=//'`
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key Description --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactive --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactiverule --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key title --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key titlematch --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclass --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclasscomplete --delete &
				kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclassmatch --delete &
				kwriteconfig5 --file kwinrulesrc --group "General" --key count "$(($COUNT - 1))"
				check_error "kwinrulesremove" "$?"

			fi

			# Remove config
			printf "${GC}  REMOVING ${SETTINGS}${NC}\n"
			rm ${SETTINGS}

			printf "${GC}\nGW2Taco_Linux has been reset.${NC}\n\v"
			read -n 1 -s -r -p "Press any key to restart installer"
			check_install
			
		fi
		taco_man

	fi
	if [ "$c1" == "---" ]; then
		collect_info
	fi
	taco_man
}
#------------------------------------------------------------------------#
#------------------------ START BY GATHERING INFO ----------------------#
collect_info()
{	
	#--- NOTICE
	printf "${GC}       	       ----------${YC} Note:${GC} ---------- \n" 
	printf "        ${YC} The installer has not been thoroughly tested. ${GC} \n"
	printf "${YC} If you encounter any errors, check the issue board on${BC} GitLab${GC} \n\v"

	#--- DISTRIBUTION
	printf "Before we proceed we need to know a few things...\n"
	printf "${GC}\nWhat distribution are you running?${NC}\n\v"
	printf "[1] Debian/Ubuntu/Mint\n"
	printf "[2] Arch/Manjaro\n\v"

	read -rep "Enter [#]: " -n 1 s1

	#--- DEKSTOP ENVIRONMENT
	show_header "tacoman" "installer"
	printf "${GC}\nWhat is your Desktop Environment?${NC}\n\v"
	printf "[1] XFCE\n"
	printf "[2] KDE\n"
	printf "[3] Cinnamon\n"
	printf "[4] GNOME\n\v"

	read -rep "Enter [#]: " -n 1 s2

	#--- COMPOSITOR
	show_header "tacoman" "installer"
	printf "${GC}\nAre you using a custom compositor or the default one?${NC}\n"
	printf "(if you do not know what this means, choose default)\n\v"
	printf "[1] Default\n"
	printf "[2] Custom\n\v"

	read -rep "Enter [#]: " -n 1 s3

	#--- WINE
	show_header "tacoman" "installer"
	printf "${GC}\nWhich WINE binaries are you using?${NC}\n\v"
	printf "[1] ArmordVehicle's Portable Package\n"
	printf "[2] Native WINE\n"
	printf "[3] Lutris WINE\n\v"

	read -rep "Enter [#]: " -n 1 s4

	#======= CHOICE VARS ===============#
	case "$s1" in						#
		1) DIST="Debian/Ubuntu/Mint";;	#	DISTRIBUTION
		2) DIST="Arch/Manjaro";;		#
	esac								#	
	#-----------------------------------#
	case "$s2" in						#
		1) DE="XFCE";;					#
		2) DE="KDE";;					#
		3) DE="Cinnamon";;				#	DESKTOP ENVIRONMENT
		4) DE="GNOME";;					#
	esac								#
	#-----------------------------------#
	if [ "$s3" -eq 1 ]; then			#
		case "$s2" in					#
			1) COMP="xfwm";;			#
			2) COMP="kwin";;			#
			3) COMP="muffin";;			#
			4) COMP="mutter";;			#	COMPOSITOR
		esac							#
	fi									#	
	if [ "$s3" -eq 2 ]; then			#
		COMP="Custom"					#
	fi									#
	#-----------------------------------#
	case "$s4" in                       #
		1) WINE="portable";;			#
		2) WINE="native";;				#	WINE
		3) WINE="lutris";;				#
	esac								#
	#===================================#

	#--------------------- OUTPUT SUMMARY -------------------------------#
	c1="$DIST"
	c2="$DE"
	c3="$WINE"
	show_header "tacoman" "installer"

	printf "${GC} Right, so you are on: ${CC}${DIST}${NC}\n"
	printf "${GC} Your desktop environment is: ${CC}${DE}${NC}\n"
	printf "${GC} The compositor is: ${CC}${COMP}${NC}\n"
	printf "${GC} and WINE binaries are: ${CC}${WINE}${NC}\n\v"

	if [ "$COMP" == "Custom" ]; then
		printf "${RC} WARNING: Custom compositors are currently not supported.${NC}\n\v"
	fi

	read -rep "Start with this info? (y/n): " -n 1 YESNO
	check_error "confirm" "$YESNO"

	taco_man
}
#------------------------------------------------------------------------#
#-------------- #START INSTALLING DE TWEAKS----------------------- ---#
install_tweaks()
{
	if [ "${INSTALLED}" == "Installed" ]; then
		printf "${GC}** Skipping DE Tweaks because they are already installed...${NC}\n"
		sleep 2
		return
	fi
	printf "${GC}** Starting install of DE Tweaks...${NC}"
	
	case $DE in

		"XFCE") #---------------XFCE-----------------------
			printf "\nWe will now install & configure tweaks for the${GC} XFCE${NC} environment.\n\v"
			read -n 1 -s -r -p "Press any key to continue"

			if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
				printf "${GC}\n\v** Installing the ${CC}picom${GC} compositor ${NC}\n"
				sudo apt install picom xdotool
				check_error "apt" "$?"

			else # [ "$DIST" == "Arch/Manjaro" ]
				printf "${GC}\n\v** Installing the ${CC}Picom${GC} compositor ${NC}\n"
				sudo pacman -S picom
				check_error "pacman" "$?"
			fi

			# Export Configs
			export_defaults
			check_error "tweaks" "$?"

		;;
		
		"KDE")  #---------------KDE-----------------------

			printf "\nWe will now install & configure tweaks for the ${GC}KDE${NC} environment.\n\v"
			read -n 1 -s -r -p "Press any key to continue"

			if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
				printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor, ${CC}xdotool${GC}...${NC}\n"
				sudo apt install picom xdotool
				check_error "apt" "$?"

			else # [ "$DIST" == "Arch/Manjaro" ]
				printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor & ${CC}xdotool${GC}... ${NC}\n"
				sudo pacman -S picom xdotool
				check_error "pacman" "$?"
			fi

			# Check for previous kwinrules & set correct group
			printf "${GC}\n\v** Checking for any previous kwinrules... ${NC}\n"
			COUNT=$((`kreadconfig5 --file kwinrulesrc --group "General" --key count` + 1))
			check_error "tweaks" "$?"

			# ADD GW2 kwinrule keys
			printf "${GC}** Adding GW2 kwinrules ${NC}\n"
			kwriteconfig5 --file kwinrulesrc --group "General" --key count "$COUNT" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key Description "GW2 Opacity" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactive "100" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactiverule "2" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key title "Guild Wars 2" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key titlematch "0" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclass "^(?!.*taco.*).*(?:gw2|wine).*" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclasscomplete "true" &
			kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key wmclassmatch "3"
			check_error "kwinrules" "$?"
			
			# Reset kwin
			printf "${GC}** Refreshing KWin Window Manager ${NC}\n"
			qdbus org.kde.KWin /KWin reconfigure
			check_error "tweaks" "$?"

			# Export configs
			export_defaults
			printf "${GC}** Exporting kwinrule count ${NC}\n"
			printf "\nkwinrule=$COUNT" >> "${DIR}/${SETTINGS}"
			check_error "tweaks" "$?"

		;;

		"Cinnamon") #---------------CINNAMON-----------------------
			printf "\nWe will now install & configure tweaks for the${GC} Cinnamon${NC} environment.\n\v"
			read -n 1 -s -r -p "Press any key to continue"

			if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
				printf "${GC}\n\v** Installing ${CC}picom${GC} compositor, ${CC}Openbox${GC}...${NC}\n"
				sudo apt install picom openbox xdotool
				check_error "apt" "$?"

			else # [ "$DIST" == "Arch/Manjaro" ]
				printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor & ${CC}Openbox${GC} window manager... ${NC}\n"
				sudo pacman -S picom openbox xdotool
				check_error "pacman" "$?"
			fi

			# Export configs
			export_defaults

		;;

		"GNOME") #--------------- GNOME -----------------------
			printf "\nWe will now install & configure tweaks for the${GC} GNOME${NC} environment.\n\v"
			read -n 1 -s -r -p "Press any key to continue"

			if [ "$DIST" == "Debian/Ubuntu/Mint" ]; then
				printf "${GC}\n\v** Installing ${CC}picom${GC} compositor, ${CC}Openbox${GC}...${NC}\n"
				sudo apt install picom openbox xdotool
				check_error "apt" "$?"

			else # [ "$DIST" == "Arch/Manjaro" ]
				printf "${GC}\n\v** Installing ${CC}Picom${GC} compositor & ${CC}Openbox${GC} window manager... ${NC}\n"
				sudo pacman -S picom openbox xdotool
				check_error "pacman" "$?"
			fi

			# Export configs
			export_defaults

		;;

		*)
			printf "\nYour Desktop Environment is currently not supported.\nTry installing manually or waiting for an update...\n\v"
			#rm ${SETTINGS}
			exit 2
		;;

	esac

	# Link run_taco.sh script to portable package
	if [ "$WINE" == "portable" ]; then
		printf "${GC}** Linking into ../bin/user_run... ${NC}\n"
		sed -i '$ s/$/\ \&\ \.\.\/\.\.\/\.\.\/gw2taco\/run_taco.sh/' ../bin/user_run
		check_error "tweaks" "$?"
	fi

	sleep 2 #safety delay
	finish_install
}
#--------------------------------------------------------------------------#
#-------------------- FINISH INSTALL & SET AUTOSTART ---------------------#
finish_install()
{
	read -rep "Would you like TacO to start automatically with GW2? (y/n): " -n 1 AUTO
	check_error "autostart"

	show_header "tacoman" "complete"

	#PORTABLE
	if [ "$WINE" == "portable" ]; then

		if [[ "$AUTO" =~ ^[Yy]$ ]]; then
			printf "\nautostart=true" >> ${SETTINGS}
			printf "\nTACO is set to autostart\n"
		else
			printf "\nautostart=false" >> ${SETTINGS}
			printf "\nTACO is NOT set to autostart.\nYou will first have to start GW2 and then use${YC} run_taco.sh${NC}"
		fi

		printf "\nTo change configurations, edit${CC} ${SETTINGS}${NC}"
	
	fi

	#NATIVE
	if [ "$WINE" == "native" ]; then

		if [[ "$AUTO" =~ ^[Yy]$ ]]; then
			printf "\nautostart=true" >> ${SETTINGS}
			printf "${RC}\nNOTE: Autostart for native WINE is experimental.${NC}\nYou will have to use run_taco.sh instead of starting GW2.exe, it will start it for you.\n If issues arise then set autostart=false and use${YC} run_taco.sh${NC} to activate taco AFTER having started GW2 and loaded a map"
			printf "\nTo change startup time & opacity, edit${BC} ${SETTINGS}${NC}"
		else
			printf "\nautostart=false" >> ${SETTINGS}
			printf "${RC}\nNOTE: Native WINE support is currently experimental.${NC}\nIf issues arise please submit it on GitLab.\nTo activate taco, first start GW2, load a map and then use${YC} run_taco.sh${NC}"
			printf "\nTo change startup time & opacity, edit${BC} ${SETTINGS}${NC}"
		fi

	fi

	#LUTRIS
	if [ "$WINE" == "lutris" ]; then

		if [[ "$AUTO" =~ ^[Yy]$ ]]; then
			printf "\nautostart=true" >> ${SETTINGS}
			printf "\ngameid=0" >> ${SETTINGS}
			printf "${YC}\nNOTE: Lutris support is currently experimental. You must change gameid=0 to the gameid of guildwars2 in lutris.${NC}\nIf issues arise please submit it on GitLab.\nYou will have to use run_taco.sh, it will start GW2 for you via lutris it for you.\n If issues arise then set autostart=false and use${YC} run_taco.sh${NC} to activate taco AFTER having started GW2 and loaded a map"
			printf "\nTo change startup time & opacity, edit${BC} ${SETTINGS}${NC}"
		else
			printf "\nautostart=false" >> ${SETTINGS}
			printf "\ngameid=0" >> ${SETTINGS}
			printf "${YC}\nNOTE: Lutris support is currently experimental. You must change gameid=0 to the gameid of guildwars2 in lutris if you want to use autostart.${NC}\nIf issues arise please submit it on GitLab.\nTo activate taco, first start GW2, load a map and then use${YC} run_taco.sh${NC}"
			printf "\nTo change startup time & opacity, edit${BC} ${SETTINGS}${NC}"
		fi

	fi

	printf "${GC}\n-------------------------------------------------${NC}\n\v"

	read -n 1 -s -r -p "Press any key to continue"
	return
}
#-------------------------#

run_script()
{
	#check_error			# Error checker
	#export_defaults		# Exports user info & default configs
	#fix_transparent_css	# Fixes transparent taco menu
	#show_header			# Shows the header
	#show_footer			# Shows the footer
	check_install			# Checks if already installed and offers uninstall
	#collect_info			# Starts by asking for environment info
	#taco_man				# Taco Manager
	#install_tweaks			# Starts installing tweaks based on environment
	#finish_install			# Finish install by exporting last configs & setting autostart=true/false
}
run_script # Initialize