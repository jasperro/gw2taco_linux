Current Versions
================

* gw2taco_linux (v1.2.2)
* gw2taco (v047.2802r)

* Tekkit's Workshop's markers (v?)
* GW2TacO ReActif Pack (v?)
* TacO MegaPack Project (v2017.10.19_36116)
* Tiscan's markers (v?)

Changelog
=========

#### ---------- 2021/01/05 - v1.3.0 ----------

* Implemented version checking when run_taco.sh is initialized
* Partial implementation of built in version updates

#### ---------- 2021/01/05 - v1.2.2 ----------

* BugFix: GLSL path was not escaped properly
* Set picom backend to glx as default for opacity mode.

#### ---------- 2020/12/25 - v1.2.1 ----------

* BugFix: lutris: grep pulls dxvk_version instead of wine, script fails

#### ---------- 2020/12/25 - v1.2.0 ----------

* GNOME was using cinnamon tweaks causing multiple errors. Reimplemented GNOME as a seperate tweak.
* BugFix: Enclosed a few vars.

#### ---------- 2020/12/24 - v1.1.1 ----------

* BugFix: lutris does not work in mode=transparency

#### ---------- 2020/12/24 - v1.1.0 ----------

* Added Lutris support
* BugFix: xdotool missing from apt install on XFCE in script

#### ---------- 2020/12/13 - v1.0.2 ----------

* BugFix: Taco Menu is transparent

#### ---------- 2020/12/03 - v1.0.1 ----------

* BugFix: Picom failed to start because it executed too fast

#### ---------- 2020/12/01 - v1.0.0 (Beta Release) ----------

* Migrated from compton to picom
* Added xdotool for window operations
* Moved configs to config folder
* Picom has config which is customizable, with however some settings take priority in TacoRunner
* Implemented experimental transparency
* Added Opacity/Transparency mode switch
* Improved code syntax
* Improved Error checks and notices
* Minimized TacoRunner code and merged Arch/Deb distributions (for now) assuming that the operations performed in the DE's of those Distributions are the same.
* Same as above for GNOME/Cinnamon
* Target windowids directly instead of relying on active-opacity
* Added safety delays to prevent conflicts between execution of commands
* BugFix: Errors when starting TacoRunner without having an "Active" tacopack
* BugFix: Changed if [ $var = "" ] to [ var == "" ]
* Set default mode to opacity upon install
* Started consolidating peices of code for re-usability
* Changed naming structure, **TacoMan** refers to ***install_taco.sh***, **TacoRunner** refers to ***run_taco.sh*** and their contents

#### ---------- 2020/11/03 - v0.9a ----------

* Improved installer, error checks & notices
* Removed the (seperate) download manager and merged function into TacoMan
* Reworked TacoMan codebase to be able to manage & install taco packs
* Added ability to have multiple tacopacks installed and switch between them easily
* All Tacopacks are now downloaded/stored in their own subfolders
* Offical GW2tacO will be downloaded & installed automatically with tacopacks that have it as a dependancy
* TacoMan will skip sections if package is already downloaded or DE Tweaks installed.
* TacoMan is able to refresh itself on navigation, allowing ui updates when editing config files or packages
* Tacopack info is now pulled from taco.links file
* Some prep for v1.0 beta release
* Added GNOME support

#### ---------- 2020/09/06 - v0.8a ----------

* Fixed bug where kwinrules failed to set opacity because of decimal value
* Fixed bug where you would return to tacoman if chose to exit after install
* Stabilized runtime and added autostart support for native wine
* Better use of variables in the installer
* Better notices in runtime

#### ---------- 2020/09/05 - v0.7a ----------

* More error checking improvements
* Better navigation & UI. TacoMan fully integrated
* Reorganised and improved code in run_taco.sh

#### ---------- 2020/09/05 - v0.6a ----------

* More UI/Error checking improvements
* Better navigation & UI. (Prep for 'TacoMan' module)
* More information is displayed on each download page

#### ---------- 2020/09/04 - v0.5a-1 ----------

* Bug Fix - Taco failed to start, could not find settings.conf, was changing DIR too late

#### ---------- 2020/09/04 - v0.5a ----------

* Completely redefined UI and code structure
* Now able to Navigate through the installer and use it as a manager
* Implemented the ability to download & install tackpacks from witin the UI
* taco.links file stores URLs, able to update if needed
* Option to download a custom tacopack besides the 5 that are built in, by providing the URL in taco.links
* Added Manual & Autostart taco startup times to settings.conf (can adjust as necessary)
* Disabled downloading/install of tacopacks until next release (not fully implemented)
* Implemented the Taco PacMan, able to view, download & manage all taco packs
* Added better uninstall/reset options
* Much better error checking, reports & installer activity notices


#### ---------- 2020/09/02 - v0.4a ----------

* GitLab upload / Git version
* Added documentation (readme, license, contributing, changelog)
* Fixed taco not starting when autostart=false on wine=portable
* Fixed regex for kwinrules
* Added gw2/taco dir paths to config
* Updated opacity defaults
* Lowered wait time from 30s to 5s when starting taco manually
* Added downloader/taco.zip extraction functions
* Improved UI

#### ---------- 2020/08/15 - v0.3a ----------

* Arch support for XFCE, KDE & Cinnamon (experimental untested)
* Patched an issue where cd command was failing if $DIR path had spaces
* Loosened KDE's kwinrules matching strictness & added regex
* Removed redundant .log file from /app
* Slightly more informative errors

#### ---------- 2020/08/05 - v0.2a ----------

* KDE support
* Cinnamon support
* Removed Arch support temporarily
* Improved the script logic
* Added option for running TACO when not using the portable package
* Replaced printf with sed for hooking into the portable package

#### ---------- 2020/06/26 - v0.1a (Alpha Release) ----------

* Initial release, based on GW2TacO Build 047.2802r
* XFCE support
    -> Debian
    -> Ubuntu/Mint (untested)
    -> Arch/Manjaro (untested)
* Dynamic Installer
    -> Able to hook into the portable package's wine binaries and autostart with GW2
    -> Able to unhook from package and leave no trace.
* Toggles compositor settings for you on run and resets your defaults on exit
* A config file for changing opacity/autostart settings easily